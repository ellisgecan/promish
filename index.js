'use strict';

return module.exports = Promish;

// returns a pending promise that can be resolved by calling the .resolve,
// and .reject methods attached to it
// useful for those a --> b --> a situations
// resolution will occur asynchronously after resolve or reject are called
function Promish() {

  let pending = true;
  let resolve, reject;

  const p = new Promise((...args) => {
    [resolve, reject] = args;
  });

  p.resolve = resolve;
  p.reject = reject;
  p.callback = callback;
  
  p.async = async;
  
  return p;
  
  function callback(err, data) {
    if(err) reject(err);
    else resolve(err);
  }
  
  function async(cb) {
    p.catch(cb);
    p.then(function(data) {
      cb(null, data);
    });
  }
}
